package br.com.williamsgomes.springbootproperties.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import br.com.williamsgomes.springbootproperties.domain.Propriedade;

public interface PropriedadeRepository extends PagingAndSortingRepository<Propriedade, String> {

	@Query("SELECT p FROM Propriedade p WHERE p.nome like %:filtro% ORDER BY categoria, subcategoria, nome")
	public List<Propriedade> findByFiltro(@Param("filtro") String filtro);
	
}
